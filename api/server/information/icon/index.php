<?php
    include($_SERVER['DOCUMENT_ROOT'] . '/api/query/status.class.php');
    $status = new MinecraftServerStatus(); // call the class
    
    $raw = $_GET['raw'];

    $response = $status->getStatus('dgpvp.com');

    $imageData = $response['favicon'];
    
    if ($raw == "true") {
        echo $imageData;
    } else {
        echo "<img width=\"64\" height=\"64\" src=\"".$imageData."\" />";
    }
?>