<?php
        
    $ip = "http://localhost";

    $isOnline = file_get_contents($ip . '/api/server/information/isOnline');
    $motd = file_get_contents($ip . '/api/server/information/motd');
    $ping = file_get_contents($ip . '/api/server/information/ping');
    $version = file_get_contents($ip . '/api/server/information/version');
    $playercount = file_get_contents($ip . '/api/server/information/playercount');
    $maxplayers = file_get_contents($ip . '/api/server/information/maxplayers');
    $tps = file_get_contents($ip . '/api/server/information/tps');
    $onlinestaff = file_get_contents($ip . '/api/server/information/staff');
    $imageData = file_get_contents($ip . '/api/server/information/icon/?raw=false');    
?>

<html>
    
    <head>
        <meta charset="utf-8">
        
        <title>DGPvP Status Page</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    	<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
    	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        
    </head>
    
    <body>
        <div class="container">
        <h1>DgPvP Network</h1><hr>       
		<div class="row">
			<div class="col-md-4">
				<h3>Bungee Server</h3>
				<table class="table table-striped">
					<tbody>
						
                        <tr>
							<td><b>Favicon</b></td>
							<td><?php echo $imageData; ?></td>
						</tr>
                        
                        <tr>
							<td><b>IP</b></td>
							<td>DgPvP.com</td>
						</tr>
                        
                        <tr>
                            <td><b>MOTD</b></td>
                            <td><?php echo $motd; ?></td>
                        </tr>
						
                        <tr>
							<td><b>Version</b></td>
							<td><?php echo $version; ?></td>
						</tr>
						
                        <tr>
							<td><b>Players</b></td>
							<td><?php echo $playercount; ?> / <?php echo $maxplayers; ?></td>
						</tr>
                        
                        <tr>
                            <td><b>Staff online</b></td>
                            <td><?php echo $onlinestaff; ?></td>
                        </tr>
						
                        <tr>
							<td><b>Status</b></td>
							<td><?php if ($isOnline == "true") { echo "Server Online!"; } else { echo "Server Offline!"; } ?></td>
						</tr>
				                                
                        <tr>
                            <td><b>Ping</b></td>
                            <td><?php echo $ping; ?> ms</td>
                        </tr>
                        
                        <tr>
                            <td><b>TPS</b></td>
                            <td><?php echo $tps; ?></td>
                        </tr>
                        
                        <tr>
                            <td><b>Reboot</b></td>
                            <td><form><input type="submit" value="Reboot"></form></td>
                        </tr>
                                                                        
                        <tr>
                            <td><b>Refresh</b></td>
                            <td><form><input type="submit" value="Refresh"></form></td>
                        </tr>
				    
                    </tbody>
				</table>
			</div>
			<div class="col-md-8" style="font-size:0px;">
				<h3>Server Statuses</h3>
				<div class="alert alert-danger" style="font-size:16px;"> <?php echo $onlinestaff; ?> </div>			
            </div>
		</div>
	</div>
    </body>
</html>