# README #

This is a web control panel for DgPvP servers.

**To do:**


- Find per server information eg: Playercount on prison or factions


- Don't ping the server, from the server, as this will only ever yeild a ping of 1ms (This isn't really a programming problem, but more of a problem that we are running the status page on the server instead of on it's own dedicated machine)


- See if specific MC server is down or entire dedi is (Maybe ping dedi server itself to see if online)